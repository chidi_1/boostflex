$(document).ready(function () {
    $('.js--toggle-menu').on('click', function () {
        $('.header').toggleClass('open');
        return false;
    });

    $('.js--select-style').styler({
        onSelectClosed: function () {
            var select = $(this);
            if (select.hasClass("platform")) {
                var clas = select.find('option:selected').attr('class')

            }
        }
    });

    $('.popup__psevdoselect').each(function () {
        var input = $(this);
        input.find('span').text(input.next('.hidden-block').find('option:selected').text());
    });
    
    $('.js--change-input').on('click', function () {
        var btn = $(this);
        var container = btn.parents('.two-third');
        var index = container.find('option:selected').index();
        var next_index;
        var length =  container.find('option').length;

        if(btn.hasClass('popup__prev')){
            next_index = index - 1;
        }
        else{
            next_index = index + 1;
        }

        if(next_index > 0){
            container.find('.popup__prev').removeClass('disabled')
        }
        else{
            container.find('.popup__prev').addClass('disabled')
        }

        if(next_index < length - 1){
            container.find('.popup__next').removeClass('disabled')
        }
        else{
            container.find('.popup__next').addClass('disabled')
        }

        container.find('option').removeProp('selected').eq(next_index).prop('selected', 'selected');
        container.find('span').text(container.find('option:selected').text());

        count_price();

        return false;
    });
    
    function count_price() {
        var price = Number($('.popup .main-form').data('price'))
        var player = Number($('.js--player option:selected').val())
        var hour = Number($('.js--hour option:selected').val())
        var val = price * player * hour;

        $('.popup').find('.popup__price_val').text('$' + val)
        $('.popup').find('.js--form-submit  span').text('$' + val)
    }

    $('.js--close').on('click', function () {
        $('.fancybox-close-small').trigger('click');
        return false;
    })

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $(document).on('click', '.js--form-submit', function () {
        var btn = $(this);
        var form = btn.closest('.main-form');
        var errors = false;

        $(form).find('.required').each(function () {
            var inp = $(this);
            var val = inp.prop('value');
            if (val == '') {
                inp.addClass('error');
                errors = true;
            } else {
                if (inp.hasClass('inp-mail')) {
                    if (validateEmail(val) == false) {
                        inp.addClass('error');
                        errors = true;
                    }
                }
            }
        });

        if (errors == false) {
            var button_value = btn.find('span').html();
            btn.text('Подождите...');

            var method = form.attr('method');
            var data = form.serialize();
            var action = form.attr('action');

            $.ajax({
                type: method,
                url: action,
                data: data,
                success: function (data) {
                    form.trigger('submit');
                },
                error: function (data) {
                    btn.text('Ошибка');
                    setTimeout(function () {
                        btn.html(button_value);
                    }, 2000);
                }
            });
        }

        return false;
    });

    $('.inp').focus(function () {
        $(this).removeClass('error');
    });

    $('.user__slider').owlCarousel({
        margin: 0,
        loop: true,
        nav: true,
        navText: [,],
        dots: true,
        autoplay: false,
        items: 1,
        onInitialized: function () {
            $('.user__slider').addClass('owl-carousel')
        }
    });

    $('.js--next-step').on('click', function () {
        $('.popup__step').toggleClass('active');
        return false;
    })

});